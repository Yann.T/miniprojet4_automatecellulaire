import unittest
from script import *

class TestScriptMethods(unittest.TestCase):
    def test_Get_Taille_carte(self):
        carte = [[0 for colonne in range(5)] for ligne in range(10)] 
        horizontal, vertical = Get_Taille_carte(carte)
        self.assertEqual(horizontal,10)
        self.assertEqual(vertical,5)

        carte = [[0 for colonne in range(1)] for ligne in range(1)] 
        horizontal, vertical = Get_Taille_carte(carte)
        self.assertEqual(horizontal,1)
        self.assertEqual(vertical,1)

    def test_init_Carte(self): 
        carte = init_Carte(3, 3, 0) #Permet de tester si la carte est bien Vide
        self.assertEqual(carte[0][0], 0)
        self.assertEqual(carte[1][1], 0)
        self.assertEqual(carte[2][2], 0)

        carte = init_Carte(3, 3, 1) #Permet de tester si la carte est bien une Forêt
        self.assertEqual(carte[0][0], 3)
        self.assertEqual(carte[1][1], 3)
        self.assertEqual(carte[2][2], 3)

    def test_init_Feu(self): 
        carte = init_Carte(3, 3, 1)

        init_Feu(1, 1, carte)
        self.assertEqual(carte[1][1], 2)

        init_Feu(0, 0, carte)
        self.assertEqual(carte[0][0], 2)

    def test_feu_Present(self):
        self.assertFalse(feu_Present(3, 1))#forêt non modifiée
        self.assertFalse(feu_Present(3, 0))#forêt modifiée

        self.assertTrue(feu_Present(2, 1))#feu non modifié
        self.assertFalse(feu_Present(2, 0))#feu modifié

        self.assertFalse(feu_Present(1, 1))#cendre non modifiée
        self.assertFalse(feu_Present(1, 0))#cendre modifiée

        self.assertFalse(feu_Present(0, 1))#terre non modifiée
        self.assertFalse(feu_Present(0, 0))#terre modifiée

    def test_propagation_Feu_Un_Tour(self):
        carte = init_Carte(3, 3, 1)
        init_Feu(1, 1, carte)

        propagation_Feu_Un_Tour(carte, False)#test de tout les cellules de la carte pour voir si le feu s'est bien propagé
        self.assertEqual(carte[1][1], 1) #test si la cellule s'est bien transformée en cendre
        self.assertEqual(carte[0][1], 2)
        self.assertEqual(carte[2][1], 2)
        self.assertEqual(carte[1][0], 2)
        self.assertEqual(carte[1][2], 2)
        self.assertEqual(carte[0][0], 3)
        self.assertEqual(carte[0][2], 3)
        self.assertEqual(carte[2][0], 3)
        self.assertEqual(carte[2][2], 3)

        propagation_Feu_Un_Tour(carte, False)
        self.assertEqual(carte[1][1], 0) #test si la cellule s'est bien transformée en terre
        self.assertEqual(carte[0][1], 1)
        self.assertEqual(carte[2][2], 2)

if __name__ == '__main__':
    unittest.main()